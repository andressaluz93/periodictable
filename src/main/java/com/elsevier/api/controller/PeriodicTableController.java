package com.elsevier.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

import com.elsevier.api.dto.PeriodicTableAtomicNumberDto;
import com.elsevier.api.dto.PeriodicTableDto;
import com.elsevier.api.model.PeriodicTable;
import com.elsevier.api.repository.PeriodicTableRepository;

@RestController
@RequestMapping("/")
public class PeriodicTableController {
	@Autowired
	private PeriodicTableRepository periodicTableRepository; 
	
	@GetMapping
	public List<PeriodicTableDto> getAllPeriodicTable(){
		List<PeriodicTable> newList = periodicTableRepository.findAll();
		return PeriodicTableDto.ConverterPeriodicTable(newList);
	}
	@GetMapping("atomicNumber/{id}")
	public PeriodicTableAtomicNumberDto getAtomicNumber(@PathVariable String id) {
		PeriodicTable periodicTable = periodicTableRepository.findAtomicNumber(id);
		if (!periodicTable.getAlternative_name().equalsIgnoreCase("n/a")) {
			periodicTable.setAlternative_name("none");
		}
		if(periodicTable.getDiscovery().equalsIgnoreCase("n/a")) {
			periodicTable.setDiscovery("unknown");
		}
		if (periodicTable.getGroup_block().equalsIgnoreCase("n/a")) {
			periodicTable.setGroup_block("0");
		}
		return new PeriodicTableAtomicNumberDto(periodicTable);
	}
	@GetMapping("period/{id}")
	public List<PeriodicTable> getPeriod(@PathVariable String id){
		List<PeriodicTable> listPeriod = periodicTableRepository.findAllByPeriod(id);
		return listPeriod;
	}
	@GetMapping("group/{id}")
	public List<PeriodicTable> getGroup(@PathVariable String id){
		List<PeriodicTable> listGroup = periodicTableRepository.findAllByGroup(id);
		return listGroup;
	}
	
	@PostMapping
	public void insertPeriodicTable(@RequestBody List<PeriodicTable> form, UriComponentsBuilder componentsBuilder) {
		periodicTableRepository.saveAll(form);
	}
	
}
