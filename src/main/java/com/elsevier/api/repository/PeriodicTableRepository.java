package com.elsevier.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.elsevier.api.model.PeriodicTable;

public interface PeriodicTableRepository extends JpaRepository<PeriodicTable, Long>{

	@Query(value = "SELECT * FROM PERIODIC_TABLE WHERE atomic_number = :atomicNumber", nativeQuery = true)
	PeriodicTable findAtomicNumber(@Param("atomicNumber") String atomicNumber);

	List<PeriodicTable> findAllByPeriod(String id);
	
	@Query(value="SELECT * FROM PERIODIC_TABLE WHERE group_block LIKE %:groupBlock%", nativeQuery = true)
	List<PeriodicTable> findAllByGroup(String groupBlock);
}
