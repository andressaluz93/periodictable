package com.elsevier.api.dto;

import java.util.List;
import java.util.stream.Collectors;
import com.elsevier.api.model.PeriodicTable;
import lombok.Getter;
@Getter
public class PeriodicTableAtomicNumberDto {
	private String atomicNumber;
	private String alternative_name;
	private String appearance;
	private String discovery;
	private String discovery_and_first_isolation;
	private String group_block;
	
	public PeriodicTableAtomicNumberDto(PeriodicTable periodicTable) {
		this.atomicNumber = periodicTable.getAtomic_number();
		this.alternative_name = periodicTable.getAlternative_name();
		this.appearance = periodicTable.getAppearance();
		this.discovery = periodicTable.getDiscovery();
		this.discovery_and_first_isolation = periodicTable.getDiscovery_and_first_isolation();
		this.group_block = periodicTable.getGroup_block();
	}
	public static List<PeriodicTableDto> ConverterPeriodicTable(List<PeriodicTable> periodicTable){
		return periodicTable.stream().map(PeriodicTableDto::new).collect(Collectors.toList());
	}
}
