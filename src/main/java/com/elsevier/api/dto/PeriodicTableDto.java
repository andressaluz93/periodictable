package com.elsevier.api.dto;

import java.util.List;
import java.util.stream.Collectors;

import com.elsevier.api.model.PeriodicTable;

import lombok.Getter;

@Getter
public class PeriodicTableDto {
	
	private String name;
	private String atomicNumber;
	
	public PeriodicTableDto(PeriodicTable periodicTable) {
		this.name = periodicTable.getName();
		this.atomicNumber = periodicTable.getAtomic_number();
	}
	
	public static List<PeriodicTableDto> ConverterPeriodicTable(List<PeriodicTable> periodicTable){
		return periodicTable.stream().map(PeriodicTableDto::new).collect(Collectors.toList());
	}
}
