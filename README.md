Definitions:

- Java 8
- SpringBoot
- DataBase H2 (memory DataBase), you can access on: localhost:8080/h2-console : more information see application.properties.

EndPoints:
Method GET: localhost:8080/
return the Name and Atomic Number properties of elements list in the table

Method GET: localhost:8080/atomicNumber/{id}
return the Name, Alternative name, Appearance, Discovery, Discovery_and_first_isolation and group_block properties of elements list with same id in the table

Method GET: localhost:8080/period/{id}
return all properties of elements list in the table with same id as the period field.

Method GET: localhost:8080/group/{id}
return all properties of elements list in the table with same id as the group field.

Method POST: localhost:8080/
with the body of the same structure as the file periodic_table(1).json


